<?php

namespace Controller;

use Form\LoginUser;
use Model\DocumentRepository;
use Model\EmployeeRepository;
use Model\ProjectRepository;

class DefaultController extends BaseController
{
    public function __construct()
    {
        $this->overrideAcl = true;
        parent::__construct();
    }

    public function homepageAction()
    {
        $this->render('default:homepage');
    }

    public function loginAction()
    {

        $form = new LoginUser();

        if ($_POST) {
            $form->authenticate($_POST);
        }

        $this->render('default:login', $form);
    }
}