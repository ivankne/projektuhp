<?php

namespace Controller;

class BaseController
{
    protected $overrideAcl = false;

    public function __construct()
    {
        if (!isset($_SESSION['userId']) && !$this->overrideAcl) {
            die('reka sam ne može');
        }
    }

    protected function render($view, $vars = null)
    {
        $viewExploded = explode(':', $view);

        require_once('./View/' . $viewExploded[0] . '/' . $viewExploded[1] . '.view.php');
    }
}