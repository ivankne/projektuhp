<?php

namespace Controller;


use Form\CreateCompany;
use Model\Company;
use Model\CompanyRepository;

class CompanyController extends BaseController
{
    public function createAction()
    {
        $form = new CreateCompany();

        if ($_POST) {
            $company = new Company();
            $company->setName($_POST['name']);
            $company->setAddress($_POST['address']);
            $company->setEmail($_POST['email']);
            $company->setOib($_POST['oib']);

            $companyRepository = new CompanyRepository();
            $companyRepository->saveCompany($company);

            die('tralalala');
        }

        $this->render('company:create', $form);
    }

    public function readAction()
    {

    }

    public function updateAction()
    {

    }

    public function deleteAction()
    {

    }
}