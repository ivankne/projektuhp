<?php

namespace Controller;

use Model\User;
use Model\UserRepository;

class UserController extends BaseController
{
    public function createAction()
    {
        $form = new \Form\RegisterUser();

        if ($_POST) {

            $form->validate($_POST);

            $user = new User();
            $user->setFirstName($_POST['firstName']);
            $user->setLastName($_POST['lastName']);
            $user->setUsername($_POST['username']);
            $user->setEmail($_POST['email']);
            $user->hashAndSetPassword($_POST['password']);

            $userRepository = new UserRepository();
            $userId = $userRepository->saveUser($user);

        }

        $this->render('user:create', $form);
    }

    public function updateAction()
    {


        if (!isset($_GET['id'])) {
            die('nema id-a');
        }

        $userId = $_GET['id'];
        $userRepository = new UserRepository();
        $user = $userRepository->getById($userId);

        if (!$user) {
            die('nema usera');
        }

        if ($_POST) {

            $user->setFirstName($_POST['firstName']);
            $user->setLastName($_POST['lastName']);
            $user->setUsername($_POST['username']);
            $user->setEmail($_POST['email']);

            $userRepository->saveUser($user);

            echo "<pre>";
            var_dump($user);
            die('kraj posta');
        }

        $this->render('user:update', $user);
    }


    /**
     * This action provides reading of user profile
     */
    public function readAction()
    {
        $userId = isset($_GET['userId']) ? $_GET['userId'] : null;
        $userRepository = new UserRepository();
        if ($userId) {
            $vars['user'] = $userRepository->getById($userId);
        } else {
            $vars['users'] = $userRepository->getAll();
        }

        $this->render('user:read', $vars);
    }

    public function deleteAction()
    {
        if (!isset($_GET['id'])) {
            die('nema id-a');
        }

        $userId = $_GET['id'];
        $userRepository = new UserRepository();
        $user = $userRepository->getById($userId);

        if (!$user) {
            die('nema usera');
        }

        $userRepository->deleteUser($user);


        $this->render('user:delete', $user);
    }


}