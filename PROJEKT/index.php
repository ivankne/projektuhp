<?php
session_start();

require_once ('./Config/autoload.php');

$routesArray = \Config\Routes::getRoutes();

if (isset($_SERVER['PATH_INFO'])) {
    $pathTrimmed = ltrim($_SERVER['PATH_INFO'], '/');
    if (isset($routesArray[$pathTrimmed])) {
        $target = $routesArray[$pathTrimmed];
        $targetCtrlAction = explode(':', $target);

        $controllerName = '\\Controller\\' . ucfirst($targetCtrlAction[0]) . 'Controller';
        $actionName = $targetCtrlAction[1] . 'Action';


    }
}

if (!isset($controllerName) || !isset($actionName)){
    // otvori base page / homepage
    $controllerName = '\\Controller\\DefaultController';
    $actionName = 'homepageAction';
}


$controller = new $controllerName();
$controller->$actionName();