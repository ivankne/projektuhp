<?php

namespace Form\Element;


class Text extends Base
{


    public function __construct($name, $labelText = null)
    {
        parent::__construct($name, $labelText);

        $this->element .= "<input type='text'";

        if ($name) {
            $this->element .= " name='" . $name . "' ";
        }


    }



}