<?php

namespace Form\Element;

abstract class Base
{
    protected $name;
    protected $element = '';

    private $isRequired;
    private $maxChars;

    public function __construct($name, $labelText = null)
    {
        $this->name = $name;
        $this->element .= "<label>" . $labelText . "</label><br>";
    }

    /**
     * @param mixed $required
     */
    public function isRequired($required)
    {
        $this->isRequired = $required;
        if ($required) {
            $this->element .= " required ";
        }
    }

    public function getRequired()
    {
        return $this->isRequired;
    }

    /**
     * @return mixed
     */
    public function getMaxChars()
    {
        return $this->maxChars;
    }

    /**
     * @param mixed $maxChars
     */
    public function setMaxChars($maxChars)
    {
        $this->maxChars = $maxChars;
        $this->element .= " maxlength='$maxChars'";
    }



    public function getName()
    {
        return $this->name;
    }

    public function setPlaceholder($placeholder)
    {
        if ($placeholder) {
            $this->element .= " placeholder=" . $placeholder . " ";
        }
    }

    public function closeElement()
    {
        $this->element .= "/> <br> <br>    ";
    }

    public function __toString()
    {
        return $this->element;
    }

}