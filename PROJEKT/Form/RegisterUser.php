<?php

namespace Form;

use Form\Element\Base;
use Form\Element\Email;
use Form\Element\Password;
use Form\Element\Submit;
use Form\Element\Text;

class RegisterUser extends BaseForm
{


    public function init()
    {
        $firstNameElement = new Text('firstName', 'Vaše ime ovdje, gospodine/djo');
        $firstNameElement->setPlaceholder("'Unesi ime'");
        $firstNameElement->isRequired(true);
        $firstNameElement->setMaxChars(2);
        $this->addElement($firstNameElement);


        $lastNameElement = new Text('lastName', 'Vaše prezime ovdje, gospodine/djo');
        $lastNameElement->setPlaceholder("'Unesi prezime'");
        $lastNameElement->isRequired(true);
        $this->addElement($lastNameElement);

        $usernameElement = new Text('username', 'Vaše korisničko ime ovdje, gospodine/djo');
        $usernameElement->setPlaceholder("'Unesi korisničko ime'");
        $usernameElement->isRequired(true);
        $this->addElement($usernameElement);

        $emailElement = new Email('email', 'Vaš email ovdje, gospodine/djo');
        $emailElement->setPlaceholder("'Unesi email'");
        $emailElement->isRequired(true);
        $this->addElement($emailElement);

        $passwordElement = new Password('password', 'Unesite password');
        $passwordElement->setPlaceholder("'Unesi password'");
        $passwordElement->isRequired(true);
        $this->addElement($passwordElement);

        $repeatPasswordElement = new Password('repeatPassword', 'Ponovite password');
        $repeatPasswordElement->setPlaceholder("'Ponovi password'");
        $repeatPasswordElement->isRequired(true);
        $this->addElement($repeatPasswordElement);

        $submitElement = new Submit('Klik123');
        $this->addElement($submitElement);

        $this->setMethod('POST');
        $this->setAction();
    }

    public function validate($postData)
    {
        // 1. dohvati sve elemente

        $elements = $this->getElementObjects();

        /** @var Base $element */
        foreach ($elements as $element) {
            $value = $_POST[$element->getName()];

            if ($element->getRequired()) {
                if (!($value != null && $value != '')){
                    $this->setErrorMessage('Nedostaje vrijednost');
                }
            }

            if ($element->getMaxChars()) {
                if (strlen($value) > $element->getMaxChars()){
                    $this->setErrorMessage($value . ' je veća od ' . $element->getMaxChars());
                }
            }


        }


        // 2. spoji vrijednost iz $_POST ($postdata) na element

        // 3. validiraj taj input

        // 4. generiraj array sa errorima

    }

}