<?php

namespace Form;

use Form\Element\Base;
use Form\Element\Email;
use Form\Element\Password;
use Form\Element\Submit;
use Form\Element\Text;

class CreateCompany extends BaseForm
{


    public function init()
    {
        $nameElement = new Text('name', 'Ime firme');
        $nameElement->setPlaceholder("'Unesi ime firme'");
        $nameElement->isRequired(true);
        $nameElement->setMaxChars(20);
        $this->addElement($nameElement);


        $addressElement = new Text('address', 'Adresa firme');
        $addressElement->setPlaceholder("'Unesi adresu firme'");
        $addressElement->isRequired(true);
        $this->addElement($addressElement);


        $emailElement = new Email('email', 'Email firme');
        $emailElement->setPlaceholder("'Unesi email'");
        $emailElement->isRequired(true);
        $this->addElement($emailElement);

        $addressElement = new Text('oib', 'OIB');
        $addressElement->setPlaceholder("'Unesi oib'");
        $addressElement->isRequired(true);
        $this->addElement($addressElement);


        $submitElement = new Submit('Submit');
        $this->addElement($submitElement);

        $this->setMethod('POST');
        $this->setAction();
    }


}