<?php

namespace Form;

use Form\Element\Base;
use Form\Element\Email;
use Form\Element\Password;
use Form\Element\Submit;
use Form\Element\Text;
use Model\User;
use Model\UserRepository;

class LoginUser extends BaseForm
{


    public function init()
    {
        $emailElement = new Email('email', 'Vaš email ovdje, gospodine/djo');
        $emailElement->setPlaceholder("'Unesi email'");
        $emailElement->isRequired(true);
        $this->addElement($emailElement);

        $passwordElement = new Password('password', 'Unesite password');
        $passwordElement->setPlaceholder("'Unesi password'");
        $passwordElement->isRequired(true);
        $this->addElement($passwordElement);

        $submitElement = new Submit('Klik');
        $this->addElement($submitElement);

        $this->setMethod('POST');
        $this->setAction();
    }

    public function authenticate($data)
    {
        $userRepository = new UserRepository();
        /** @var User $user */
        $user = $userRepository->getOneBy(
            array('email' => $data['email'])
        );

        if ($user) {
            if (password_verify($data['password'], $user->getPassword())) {
                // ovdje znamo da je user taj za kojeg se predstavlja
                $_SESSION['userId'] = $user->getId();
            }
        }
    }

}