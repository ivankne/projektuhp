<?php

namespace Form;

use Form\Element\Base;

abstract class BaseForm
{
    private $method;
    private $action;

    private $errorMessages = array();

    private $elements = array();
    private $plainElements = array();

    abstract function init();

    public function __construct()
    {
        // generiranje elemenata
        $this->init();
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     */
    public function setAction($action = null)
    {
        $this->action = $action;
    }


    public function addElement(Base $element)
    {
        $element->closeElement();

        $this->plainElements[$element->getName()] = $element;
        $this->elements[$element->getName()] = $element->__toString();
    }

    public function getElements()
    {
        return $this->elements;
    }

    public function getElementObjects()
    {
        return $this->plainElements;
    }

    public function getElement($name)
    {
        if (isset($this->elements[$name])) {
            return $this->elements[$name];
        }

        return null;
    }

    /**
     * @return array
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param array $errorMessages
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessages[] = $errorMessage;
    }

    public function validate($postData)
    {
        // 1. dohvati sve elemente

        $elements = $this->getElementObjects();

        /** @var Base $element */
        foreach ($elements as $element) {
            $value = $_POST[$element->getName()];

            if ($element->getRequired()) {
                if (!($value != null && $value != '')){
                    $this->setErrorMessage('Nedostaje vrijednost');
                }
            }

            if ($element->getMaxChars()) {
                if (strlen($value) > $element->getMaxChars()){
                    $this->setErrorMessage($value . ' je veća od ' . $element->getMaxChars());
                }
            }


        }


        // 2. spoji vrijednost iz $_POST ($postdata) na element

        // 3. validiraj taj input

        // 4. generiraj array sa errorima

    }

}