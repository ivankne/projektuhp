<?php

namespace Config;

class Routes
{
    public static function getRoutes()
    {
        return array(
            '/' => 'default:homepage',
            'login' => 'default:login',
            // user controller
            'user/create' => 'user:create',
            'user/delete' => 'user:delete',
            'user/update' => 'user:update',
            'user/read' => 'user:read',
            // project controller
            'project/create' => 'project:create',
            'project/delete' => 'project:delete',
            'project/update' => 'project:update',
            'project/read' => 'project:read',
            // company controller
            'company/create' => 'company:create'
        );
    }
}