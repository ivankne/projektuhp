<?php

namespace Model;

class EmployeeRepository extends EntityRepository
{

    public function getProjectIds($employee)
    {
        $query = 'SELECT * FROM employee_project WHERE employee_id='. $employee->getId();

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $results = $stmt->fetchAll();

        $projectIds = array();
        foreach ($results as $result) {
            $projectIds[] = $result['project_id'];
        }

        return $projectIds;
    }

}