<?php
namespace Model;

class Employee{

    private $id;
    private $firstName;
    private $lastName;
    private $email;
    private $company_id;
    private $projects = array();

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        $companyRepository = new CompanyRepository();
        $company = $companyRepository->getById($this->company_id);

        return $company;
    }

    /**
     * @param Company $company
     */
    public function setCompany($company)
    {
        $this->company_id = $company->getId();
    }

    public function getProjects()
    {
        $employeeRepository = new EmployeeRepository();
        $projectIds = $employeeRepository->getProjectIds($this);

        $projectRepository = new ProjectRepository();
        foreach ($projectIds as $projectId) {
            $this->projects[] = $projectRepository->getById($projectId);
        }

        return $this->projects;
    }

}