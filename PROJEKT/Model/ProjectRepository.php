<?php

namespace Model;

class ProjectRepository extends EntityRepository
{
    public function getEmployeeIds($project)
    {
        $query = 'SELECT * FROM employee_project WHERE project_id='. $project->getId();

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $results = $stmt->fetchAll();

        $employeeIds = array();
        foreach ($results as $result) {
            $employeeIds[] = $result['employee_id'];
        }

        return $employeeIds;
    }



}