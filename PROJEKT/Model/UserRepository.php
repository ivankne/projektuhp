<?php

namespace Model;

class UserRepository extends EntityRepository
{
    public function deleteUser(User $user)
    {

        $statement = "DELETE FROM user WHERE id=" . $user->getId();

        $stmt = $this->conn->prepare($statement);
        $stmt->execute();

    }

    public function saveUser(User $user)
    {
        if (!$user->getId()) {
            // napravi novog usera
            $statement = "INSERT INTO user
        (firstName, lastName, username, password, email) 
        VALUES (:firstName, :lastName, :username, :password, :email)";
        } else {
            // updateaj starog usera
            $statement = "UPDATE user SET firstName=:firstName, lastName=:lastName, 
          username=:username, email=:email, password=:password WHERE id=" . $user->getId();
        }

        $stmt = $this->conn->prepare($statement);

        $stmt->bindValue(':firstName', $user->getFirstName());
        $stmt->bindValue(':lastName', $user->getLastName());
        $stmt->bindValue(':username', $user->getUsername());
        $stmt->bindValue(':email', $user->getEmail());
        $stmt->bindValue(':password', $user->getPassword());
        $stmt->execute();

        return $this->conn->lastInsertId();
    }


}