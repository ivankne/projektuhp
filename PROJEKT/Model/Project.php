<?php
namespace Model;

class Project
{

    private $id;
    private $name;
    private $startDate;
    private $endDate;
    private $description;
    private $budget;
    private $company_id;
    private $employees = array();

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * @param mixed $budget
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        $companyRepository = new CompanyRepository();
        $company = $companyRepository->getById($this->company_id);

        return $company;
    }

    /**
     * @param Company $company
     */
    public function setCompany($company)
    {
        $this->company_id = $company->getId();
    }


    public function getEmployees()
    {
        $projectRepository = new ProjectRepository();
        $employeeIds = $projectRepository->getEmployeeIds($this);

        $employeeRepository = new EmployeeRepository();
        foreach ($employeeIds as $employeeId) {
            $this->employees[] = $employeeRepository->getById($employeeId);
        }

        return $this->employees;
    }

}