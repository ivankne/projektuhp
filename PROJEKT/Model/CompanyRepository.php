<?php

namespace Model;

class CompanyRepository extends EntityRepository
{

    public function saveCompany(Company $company)
    {
        if (!$company->getId()) {

            $statement = "INSERT INTO company
        (name, address, email, oib) 
        VALUES (:name, :address, :email, :oib)";
        } else {
            // updateaj starog usera
            $statement = "UPDATE company SET name=:name, address=:address, 
          email=:email, oib=:oib WHERE id=" . $company->getId();
        }

        $stmt = $this->conn->prepare($statement);

        $stmt->bindValue(':name', $company->getName());
        $stmt->bindValue(':address', $company->getAddress());
        $stmt->bindValue(':email', $company->getEmail());
        $stmt->bindValue(':oib', $company->getOib());
        $stmt->execute();

        return $this->conn->lastInsertId();
    }

}