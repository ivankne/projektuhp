<?php

namespace Helper;

class Url
{
    public static function getUrlFromRoute($route)
    {
        $url = $_SERVER['REQUEST_URI'] . $route;

        return $url;
    }
}